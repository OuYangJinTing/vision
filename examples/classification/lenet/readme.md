# LeNet

***

## Performance

***

### Precision comparison line chart

***

## Pretrained models

***

## Training

***

### Parameter description

***

## Examples

***

### Train

- The following configuration uses 8 GPUs for training.

  ```shell
  python lenet_mnist_train.py --model lenet
  ```

output:
    TODO: @liujunyu

- The following configuration uses yaml file for training.

  ```shell
  python mindvision/classification/engine/train.py -c mindvision/config/classification/lenet_mnist.yaml
  ```

Output:
    TODO: @liujunyu

### Eval

***

### Infer

***