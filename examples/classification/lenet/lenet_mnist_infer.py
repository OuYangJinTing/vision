# Copyright 2021 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.lr_schedule.py
# org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
""" LeNet infer script. TODO @liujunyu: infer is for one image not for all val."""

import argparse
import os

import numpy as np

import mindspore.dataset as ds
from mindspore import context, Tensor
from mindspore.train import Model
import mindspore.dataset.vision.c_transforms as transforms

from mindvision.classification.models import lenet
from mindvision.dataset import DatasetGenerator
from mindvision.classification.dataset import Mnist


def lenet_infer(args_opt):
    """LeNet infer."""
    context.set_context(mode=context.GRAPH_MODE, device_target=args_opt.device_target)

    # Data pipeline.
    def read_dataset():
        img_list = list()
        id_list = list()

        idx = 0
        if os.path.isdir(args_opt.data_url):
            for img_name in os.listdir(args_opt.data_url):
                img_path = os.path.join(args_opt.data_url, img_name)
                img_list.append(img_path)
                id_list.append(idx)
                idx += 1
        else:
            img_list.append(args_opt.data_url)
            id_list.append(idx)
        return img_list, id_list

    dataset_infer = ds.GeneratorDataset(DatasetGenerator(read_dataset, mode="L"),
                                        column_names=['image', 'label'],
                                        num_parallel_workers=args_opt.num_parallel_workers)
    trans = [transforms.Resize(size=args_opt.resize),
             transforms.HWC2CHW()]
    dataset_infer = dataset_infer.map(operations=trans,
                                      input_columns='image',
                                      num_parallel_workers=args_opt.num_parallel_workers)
    dataset_infer = dataset_infer.batch(args_opt.batch_size)

    # Create model.
    network = lenet(args_opt.num_classes, pretrained=args_opt.pretrained)

    # Init the model.
    model = Model(network)

    # Begin to infer
    for data in dataset_infer.create_dict_iterator(output_numpy=True):
        image = data["image"]
        image = Tensor(np.expand_dims(image.astype(np.float32), axis=1))
        result = model.predict(image)
        result = np.argmax(result.asnumpy(), axis=1)
        for i in result:
            predict = Mnist.index2label[i]
            print("Predict: ", predict)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='LeNet infer.')
    parser.add_argument('--device_target', type=str, default="GPU", choices=["Ascend", "GPU", "CPU"])
    parser.add_argument('--data_url', required=True, default=None, help='Location of data.')
    parser.add_argument('--pretrained', type=bool, default=False, help='Load pretrained model.')
    parser.add_argument('--num_parallel_workers', type=int, default=8, help='Number of parallel workers.')
    parser.add_argument('--batch_size', type=int, default=1, help='Number of batch size.')
    parser.add_argument('--num_classes', type=int, default=10, help='Number of classification.')
    parser.add_argument('--resize', type=int, default=32, help='Resize the height and weight of picture.')

    args = parser.parse_known_args()[0]
    lenet_infer(args)
