# Copyright 2021 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#:
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
""" LeNet training script. TODO @liujunyu: add readme.md. """

import argparse

import mindspore.nn as nn
from mindspore import context
from mindspore.context import ParallelMode
from mindspore.common import set_seed
from mindspore.train import Model
from mindspore.communication import init, get_rank, get_group_size
from mindspore.train.callback import ModelCheckpoint, CheckpointConfig, LossMonitor

from mindvision.classification.dataset import Mnist
from mindvision.classification.models import lenet

set_seed(1)


def lenet_train(args_opt):
    """LeNet train."""
    context.set_context(mode=context.GRAPH_MODE, device_target=args_opt.device_target)

    # Data Pipeline.
    if args_opt.run_distribute:
        init("nccl")
        rank_id = get_rank()
        device_num = get_group_size()
        context.set_auto_parallel_context(device_num=device_num,
                                          parallel_mode=ParallelMode.DATA_PARALLEL,
                                          gradients_mean=True)
        dataset_train = Mnist(args_opt.data_url,
                              train=True,
                              batch_size=args_opt.batch_size,
                              repeat_num=args_opt.repeat_num,
                              shuffle=True,
                              num_parallel_workers=args_opt.num_parallel_workers,
                              resize=args_opt.resize,
                              num_shards=device_num,
                              shard_id=rank_id,
                              download=args_opt.download)
        ckpt_save_dir = args_opt.ckpt_save_dir + "_ckpt_" + str(get_rank()) + "/"
    else:
        dataset_train = Mnist(args_opt.data_url,
                              train=True,
                              batch_size=args_opt.batch_size,
                              repeat_num=args_opt.repeat_num,
                              shuffle=True,
                              num_parallel_workers=args_opt.num_parallel_workers,
                              resize=args_opt.resize,
                              download=args_opt.download)
        ckpt_save_dir = args_opt.ckpt_save_dir

    step_size = dataset_train.get_dataset_size()

    # Create model.
    network = lenet(args_opt.num_classes, pretrained=args_opt.pretrained)

    # Define optimizer.
    network_opt = nn.Momentum(network.trainable_params(), args_opt.learning_rate, args_opt.momentum)

    # Define loss function.
    network_loss = nn.SoftmaxCrossEntropyWithLogits(sparse=True, reduction="mean")

    # Define metrics.
    metrics = {'acc'}

    # Init the model.
    model = Model(network, loss_fn=network_loss, optimizer=network_opt, metrics=metrics)

    # Set the checkpoint config for the network.
    ckpt_config = CheckpointConfig(
        save_checkpoint_steps=step_size,
        keep_checkpoint_max=args_opt.keep_checkpoint_max)
    ckpt_callback = ModelCheckpoint(prefix='lenet',
                                    directory=ckpt_save_dir,
                                    config=ckpt_config)

    # Begin to train.
    model.train(args_opt.epoch_size,
                dataset_train,
                callbacks=[ckpt_callback, LossMonitor()],
                dataset_sink_mode=args_opt.dataset_sink_mode)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='LeNet train.')
    parser.add_argument('--data_url', required=True, default=None, help='Location of data.')
    parser.add_argument('--epoch_size', type=int, default=10, help='Train epoch size.')
    parser.add_argument('--device_target', type=str, default="GPU", choices=["Ascend", "GPU", "CPU"])
    parser.add_argument('--batch_size', type=int, default=32, help='Number of batch size.')
    parser.add_argument('--repeat_num', type=int, default=1, help='Number of repeat.')
    parser.add_argument('--num_parallel_workers', type=int, default=8, help='Number of parallel workers.')
    parser.add_argument('--num_classes', type=int, default=10, help='Number of classification.')
    parser.add_argument('--pretrained', type=bool, default=False, help='Load pretrained model.')
    parser.add_argument('--keep_checkpoint_max', type=int, default=10, help='Max number of checkpoint files.')
    parser.add_argument('--ckpt_save_dir', type=str, default="./lenet", help='Location of training outputs.')
    parser.add_argument('--learning_rate', type=float, default=0.01, help='Value of learning rate.')
    parser.add_argument('--momentum', type=float, default=0.9, help='Momentum for the moving average.')
    parser.add_argument('--dataset_sink_mode', type=bool, default=True, help='The dataset sink mode.')
    parser.add_argument('--run_distribute', type=bool, default=True, help='Distributed parallel training.')
    parser.add_argument('--resize', type=int, default=32, help='Resize the height and weight of picture.')
    parser.add_argument('--download', type=bool, default=False, help='Download Mnist train dataset.')

    args = parser.parse_known_args()[0]
    lenet_train(args)
