# Vision Transformer

***

by Alexey Dosovitskiy\*†, Lucas Beyer\*, Alexander Kolesnikov\*, Dirk
Weissenborn\*, Xiaohua Zhai\*, Thomas Unterthiner, Mostafa Dehghani, Matthias
Minderer, Georg Heigold, Sylvain Gelly, Jakob Uszkoreit and Neil Houlsby\*†.

(\*) equal technical contribution, (†) equal advising.

![Figure 1 from paper](../../../resource/classification/vit_figure.png)

Overview of the model: The paper split an image into fixed-size patches, linearly embed each of them, add position embeddings, and feed the resulting sequence of vectors to a standard Transformer encoder. In order to perform classification, authors use the standard approach of adding an extra learnable "classification token" to the sequence.

The architectural definition of each network refers to the following papers:

[1] Dosovitskiy, Alexey, et al. "An image is worth 16x16 words: Transformers for image recognition at scale." arXiv preprint arXiv:2010.11929 (2020).

## Performance

***

### Precision comparison line chart

![line_chart]()

## Pretrained models

***

The following table lists all MobilenetV2 imagenet checkpoints. Each model verifies the accuracy
of Top-1 and Top-5, and compares it with that of TensorFlow.

|  | MindSpore | MindSpore | TensorFlow | TensorFlow |||
|:-----:|:---------:|:--------:|:---------:|:---------:|:---------:|:---------:|
| Model | Top-1 (%) | Top-5 (%) | Top-1 (%) | Top-5 (%) | Download | Config |
| mobilenet_v2_1.4_224 |  |  | 75.0 | 92.5 | [model]() | [config]() |
| mobilenet_v2_1.3_224 |  |  | 74.4 | 92.1 | [model]() | [config]() |

## Training

### Parameter description

| Parameter | Default | Description |
|:-----|:---------|:--------|
| device_target | GPU | Hardware device |
| data_url |  | Path to training dataset |
| pretrained | False | Path to pretrained model |
| run_distribute | True | Distributed parallel training |
| num_parallel_workers | 8 | Number of parallel workers |
| dataset_sink_mode | True | Data sinking mode |
| num_classes | 1001 | Number of dataset classifications |
| batch_size | 128 | Number of batch size |
| repeat_num | 1 | Number of data repetitions |
| momentum | 0.9 | Momentum parameter |
| epoch_size | 100 | Number of epoch |
| keep_checkpoint_max | 10 | Maximum number of checkpoints saved |
| ckpt_save_dir | './mobilenet_v2' | Save path of checkpoint |
| lr_decay_mode | cosine_decay_lr | Learning rate decay mode |
| decay_epoch | 100 | Number of decay epoch |
| smooth_factor | 0.1 | Label smoothing factor |
| max_lr | 0.1 | maximum learning rate |
| min_lr | 0.0 | minimum learning rate |
| milestone |  | A list of milestone |
| learning_rates |  | A list of learning rates |
| alpha | 1.0 | Magnification factor |
| resize | 224 | Resize the height and weight of picture |

## Examples

### Train

- The following configuration uses 8 GPUs for training.

  ```shell
  ```

- The following configuration uses yaml file for training.

  ```shell
  ```  

### Eval

- The following configuration for eval.

  ```shell
  ```

- The following configuration uses yaml file for eval.

  ```shell
  ```

## References

In this repository release models from the papers

- [1] Dosovitskiy, Alexey, et al. "[An Image is Worth 16x16 Words: Transformers for Image Recognition at Scale.](https://arxiv.org/abs/2010.11929)" arXiv preprint arXiv:2010.11929 (2020).
- [2] Steiner, Andreas, et al. "[How to train your ViT? Data, Augmentation, and Regularization in Vision Transformers](https://arxiv.org/abs/2106.10270)." arXiv preprint arXiv:2106.10270 (2021).
