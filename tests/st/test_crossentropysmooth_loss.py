# Copyright 2021 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
""" Test cross entropy smooth loss."""

import numpy as np
import pytest

import mindspore as ms
from mindspore import Tensor
from mindvision.engine.loss import CrossEntropySmooth


@pytest.mark.level0
@pytest.mark.platform_x86_gpu_training
@pytest.mark.env_onecard
def test_crossentropysmooth_loss_gpu():
    """
    Test crossentropysmooth loss in GPU.
    """
    logits = Tensor(np.array([[3, 5, 6, 9, 12, 33, 42, 12, 32, 72]]), ms.float32)
    labels_np = np.array([[0, 0, 0, 0, 0, 0, 1, 0, 0, 0]]).astype(np.float32)
    labels = Tensor(labels_np)
    loss = CrossEntropySmooth(classes_num=2, sparse=False)
    result = loss(logits, labels)
    assert result == Tensor(np.array([30]), ms.float32)


@pytest.mark.level0
@pytest.mark.platform_arm_ascend_training
@pytest.mark.platform_x86_ascend_training
@pytest.mark.env_onecard
def test_crossentropysmooth_loss_ascend():
    """
    Test crossentropysmooth loss in ascend.
    """
    logits = Tensor(np.array([[3, 5, 6, 9, 12, 33, 42, 12, 32, 72]]), ms.float32)
    labels_np = np.array([[0, 0, 0, 0, 0, 0, 1, 0, 0, 0]]).astype(np.float32)
    labels = Tensor(labels_np)
    loss = CrossEntropySmooth(classes_num=2, sparse=False)
    result = loss(logits, labels)
    assert result == Tensor(np.array([30]), ms.float32)
