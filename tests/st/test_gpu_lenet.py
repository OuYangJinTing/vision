# Copyright 2021 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""
Test lenet network.
"""

import pytest
from test_network_main import lenet_eval
from test_network_main import lenet_train

lenet_cfg = {'data_loader': {
    'train': {'dataset': {'type': 'MnistDataset', 'dataset_dir': '', 'num_parallel_workers': 8, 'shuffle': True},
              'map': {'operations': [{'type': 'Resize', 'size': [32, 32]},
                                     {'type': 'Rescale', 'rescale': 0.004, 'shift': 0.0},
                                     {'type': 'Rescale', 'rescale': 3.246, 'shift': -0.424}, {'type': 'HWC2CHW'}],
                      'input_columns': ['image']}, 'batch': {'batch_size': 32, 'drop_remainder': True}},
    'eval': {'dataset': {'type': 'MnistDataset', 'dataset_dir': '', 'num_parallel_workers': 8, 'shuffle': True},
             'map': {'operations': [{'type': 'Resize', 'size': [32, 32]},
                                    {'type': 'Rescale', 'rescale': 0.004, 'shift': 0.0},
                                    {'type': 'Rescale', 'rescale': 3.246, 'shift': -0.424}, {'type': 'HWC2CHW'}],
                     'input_columns': ['image']}, 'batch': {'batch_size': 32, 'drop_remainder': True}},
    'group_size': 1, 'task': 'classification'},
    'model_name': 'lenet5', 'device_target': 'GPU', 'dataset_sink_mode': False,
    'context': {'mode': 0, 'device_target': 'GPU', 'save_graphs': False, 'device_id': 0},
    'model': {'type': 'BaseClassifier',
              'backbone': {'type': 'Lenet5', 'num_classes': 10, 'num_channel': 1, 'include_top': True}},
    'learning_rate': {'lr_scheduler': 'cosine_annealing', 'lr': 0.012, 'lr_epochs': [60, 70], 'lr_gamma': 0.1,
                      'eta_min': 0.0, 't_max': 100, 'max_epoch': 100, 'warmup_epochs': 4},
    'optimizer': {'type': 'Momentum', 'momentum': 0.9, 'weight_decay': 0.0004, 'loss_scale': 1024},
    'loss': {'type': 'SoftmaxCrossEntropyWithLogits', 'sparse': True, 'reduction': 'mean'},
    'train': {'pre_trained': False, 'pretrained_model': '', 'ckpt_path': './output/', 'epochs': 100,
              'save_checkpoint_epochs': 5, 'save_checkpoint_steps': 1875, 'keep_checkpoint_max': 10,
              'run_distribute': False}, 'eval': {'pretrained_model': ''},
    'infer': {'pretrained_model': '', 'batch_size': 32, 'image_shape': [1, 32, 32], 'image_path': '',
              'normalize': True, 'output_dir': './infer_output'},
    'export': {'pretrained_model': '', 'batch_size': 32, 'image_height': 32, 'image_width': 32,
               'input_channel': 1, 'file_name': 'lenet', 'file_formate': 'MINDIR'}}
train_iter = 10
train_dataset_path = "/home/workspace/mindspore_dataset/mnist/train"
eval_dataset_path = "/home/workspace/mindspore_dataset/mnist/test"
pretrained_model = "/home/workspace/mindspore_dataset/checkpoint/mindvision/lenet5.ckpt"
device = "GPU"


@pytest.mark.level0
@pytest.mark.platform_x86_gpu_training
@pytest.mark.env_onecard
def train_gpu_lenet():
    lenet_train(lenet_cfg, train_iter, train_dataset_path, device)


@pytest.mark.level0
@pytest.mark.platform_x86_gpu_training
@pytest.mark.env_onecard
def eval_gpu_lenet():
    lenet_eval(lenet_cfg, eval_dataset_path, pretrained_model, device)
