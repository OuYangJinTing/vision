# Copyright 2021 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""
The main of test network.
"""

import mindspore.nn as nn
from mindspore import context, Tensor, load_checkpoint, load_param_into_net
from mindspore.communication.management import init, get_group_size
from mindspore.context import ParallelMode
from mindspore.nn import TrainOneStepCell, WithLossCell
from mindspore.train import Model

from mindvision.check_param import Validator, Rel
from mindvision.classification.models.builder import build_classifier
from mindvision.engine.dataset.dataloader import build_dataloader
from mindvision.engine.loss.builder import build_loss
from mindvision.engine.lr_schedule.lr_schedule import get_lr
from mindvision.engine.optimizer.builder import build_optimizer
from mindvision.engine.utils.config import Config


def lenet_train(cfg, train_iter, dataset_path, device):
    """
    Train network.
    """
    config = Config(**cfg)
    context.set_context(mode=context.GRAPH_MODE,
                        device_target=device)

    # Run distribute.
    if config.train.run_distribute:
        if config.device_target == "Ascend":
            init()
        else:
            init("nccl")

        context.set_auto_parallel_context(device_num=get_group_size(),
                                          parallel_mode=ParallelMode.DATA_PARALLEL,
                                          gradients_mean=True)

    # Prepare dataset.
    config.data_loader.train.dataset.dataset_dir = dataset_path
    data_loader = build_dataloader(config.data_loader)
    dataset_train = data_loader()
    Validator.check_int(dataset_train.get_dataset_size(), 0, Rel.GT)
    batches_per_epoch = dataset_train.get_dataset_size()

    # Set network.
    network = build_classifier(config.model)

    # Set loss.
    network_loss = build_loss(config.loss)

    # Set learning rate.
    lr_cfg = config.learning_rate
    lr_cfg.steps_per_epoch = int(batches_per_epoch / config.data_loader.group_size)
    lr = get_lr(lr_cfg)

    # Set optimizer.
    config.optimizer.params = network.trainable_params()
    config.optimizer.learning_rate = lr
    network_opt = build_optimizer(config.optimizer)

    # Init the whole Model
    net_with_criterion = WithLossCell(network, network_loss)
    train_network = TrainOneStepCell(net_with_criterion, network_opt)
    train_network.set_train()

    for index, dataset in enumerate(dataset_train.create_dict_iterator(output_numpy=True)):
        data = Tensor(dataset["image"])
        label = Tensor(dataset["label"])
        res = train_network(data, label)
        assert res
        if index + 1 == train_iter:
            break


def lenet_eval(cfg, dataset_path, pretrained_model, device):
    """
    Eval network.
    """
    config = Config(**cfg)
    context.set_context(mode=context.GRAPH_MODE,
                        device_target=device)

    # Prepare dataset.
    config.data_loader.eval.dataset.dataset_dir = dataset_path
    dataset_loader = build_dataloader(config.data_loader, is_training=False)
    dataset_eval = dataset_loader()
    Validator.check_int(dataset_eval.get_dataset_size(), 0, Rel.GT)

    # Set network, loss.
    network = build_classifier(config.model)
    network_loss = build_loss(config.loss)

    # Load pretrain model.
    param_dict = load_checkpoint(pretrained_model)
    load_param_into_net(network, param_dict)

    # Define eval_metrics.
    eval_metrics = {'Top_1_Acc': nn.Top1CategoricalAccuracy()}

    # Init the whole Model.
    model = Model(network,
                  network_loss,
                  metrics=eval_metrics)

    # Begin to eval.
    acc = model.eval(dataset_eval)
    assert acc['Top_1_Acc'] > 0.96
