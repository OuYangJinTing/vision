# Copyright 2021 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
""" MindSpore Vision Classification evaluation script. """

from mindspore import context, nn, load_checkpoint, load_param_into_net
from mindspore.train import Model

from mindvision.check_param import Validator, Rel
from mindvision.classification.models.builder import build_classifier
from mindvision.engine.dataset.dataloader import build_dataloader
from mindvision.engine.loss.builder import build_loss
from mindvision.engine.utils.config import parse_args, Config


def main(pargs):
    config = Config(pargs.config)

    # set config context
    context.set_context(**config.context)

    # prepare dataset
    dataset_loader = build_dataloader(config.data_loader, types='eval')
    dataset_eval = dataset_loader()
    Validator.check_int(dataset_eval.get_dataset_size(), 0, Rel.GT)

    # set network, loss
    network = build_classifier(config.model)
    network_loss = build_loss(config.loss)

    # load pertain model
    param_dict = load_checkpoint(config.eval.pretrained_model)
    load_param_into_net(network, param_dict)

    # define eval_metrics
    eval_metrics = {'Loss': nn.Loss(),
                    'Top_1_Acc': nn.Top1CategoricalAccuracy(),
                    'Top_5_Acc': nn.Top5CategoricalAccuracy()}

    # init the whole Model
    model = Model(network,
                  network_loss,
                  metrics=eval_metrics)

    # begin to train
    print(f'[Start eval `{config.model_name}`]')
    print("=" * 80)
    acc = model.eval(dataset_eval)
    print("=" * 80)
    print("{}".format(acc))
    print(f'[End of eval `{config.model_name}`]')


if __name__ == '__main__':
    args = parse_args()
    main(args)
