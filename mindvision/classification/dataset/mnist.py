# Copyright 2021 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
""" Create the MNIST dataset """

import codecs
import os
import sys
from typing import Optional, Callable, Union, Tuple
from urllib.error import URLError
import numpy as np

import mindspore.dataset.vision.c_transforms as transforms
from mindspore.dataset.vision import Inter

from mindvision.dataset import Dataset, MetaDataset, DownLoad

__all__ = ["Mnist"]


class Mnist(Dataset, metaclass=MetaDataset):
    """
    Mnist dataset loader, the imagenet dataset after parser will look like:

        ./mnist/
        ├── test
        │   ├── t10k-images-idx3-ubyte
        │   └── t10k-labels-idx1-ubyte
        └── train
            ├── train-images-idx3-ubyte
            └── train-labels-idx1-ubyte

    Args:
        path (str): The root path of the Mnist Dataset which include train and test directory.
        train (bool): Train(True) or val(False). Default: True.
        num_parallel_workers(int, optional): Number of subprocess used to fetch the
            dataset in parallel.Default:1.
        transform (callable, optional): A function transform that takes in a image. Default:None.
        target_transform (callable, optional): A function transform that takes in a label. Default:None.
        batch_size (int): Batch size of dataset. Default:32.
        repeat_num (int): The repeat num of dataset. Default:1.
        shuffle (bool, optional): Whether or not to perform shuffle on the dataset. Default:None.
        num_parallel_workers (int, optional): Number of workers to read the data. Default: None.
        num_shards (int, optional): Number of shards that the dataset will be divided into. Default: None.
        shard_id (int, optional): The shard ID within num_shards. Default: None.
        resize (int, tuple): The output size of the resized image. If size is an integer, the smaller edge of the
        image will be resized to this value with the same image aspect ratio. If size is a sequence of length 2,
        it should be (height, width). Default: 32.
        download (bool) : Whether to download the dataset. Default: False.
    """

    def __init__(self,
                 path: str,
                 train: bool = True,
                 transform: Optional[Callable] = None,
                 target_transform: Optional[Callable] = None,
                 batch_size: int = 32,
                 repeat_num: int = 1,
                 shuffle: Optional[bool] = None,
                 num_parallel_workers: Optional[int] = None,
                 num_shards: Optional[int] = None,
                 shard_id: Optional[int] = None,
                 resize: Union[int, Tuple[int, int]] = 32,
                 download: bool = False):
        if self.download:
            self.download_dataset()

        super(Mnist, self).__init__(path=path,
                                    train=train,
                                    load_data=self.read_dataset,
                                    transform=transform,
                                    target_transform=target_transform,
                                    batch_size=batch_size,
                                    repeat_num=repeat_num,
                                    resize=resize,
                                    shuffle=shuffle,
                                    num_parallel_workers=num_parallel_workers,
                                    num_shards=num_shards,
                                    shard_id=shard_id,
                                    download=download)

    def download_dataset(self):
        """Download the MNIST data if it doesn't exist already. TODO: @liujunyu use ParseMNIST"""
        ParseMNIST()

    def default_transform(self):
        """Set the default transform for Mnist dataset."""
        rescale = 1.0 / 255.0
        shift = 0.0
        rescale_nml = 1 / 0.3081
        shift_nml = -1 * 0.1307 / 0.3081

        # define map operations
        trans = [
            transforms.Resize(size=self.resize, interpolation=Inter.LINEAR),
            transforms.Rescale(rescale, shift),
            transforms.Rescale(rescale_nml, shift_nml),
            transforms.HWC2CHW(),
        ]
        return trans


class ParseMNIST(DownLoad):
    """
    TODO @liujunyu: download content should be move to here.
    """
    mirrors = [
        'http://yann.lecun.com/exdb/mnist/'
    ]

    resources = {"train": [("train-images-idx3-ubyte.gz", "f68b3c2dcbeaaa9fbdd348bbdeb94873"),
                           ("train-labels-idx1-ubyte.gz", "d53e105ee54ea40749a09fcbcd1e9432")],
                 "val": [("t10k-images-idx3-ubyte.gz", "9fb629c4189551a2d022fa330f9573f3"),
                         ("t10k-labels-idx1-ubyte.gz", "ec29112dd5afa0611ce80d1b7f02629c")]}

    index2label = {0: 'zero', 1: 'one', 2: 'two', 3: 'three', 4: 'four',
                   5: 'five', 6: 'six', 7: 'seven', 8: 'eight', 9: 'nine'}

    def get_label2index(self):
        return self.index2label

    def check_exists(self) -> bool:
        """Check whether the file exists."""
        # TODO: @liujunyu these code is not for people reading.
        # return all(
        #     DownLoad().check_integrity(os.path.join(self.get_path, os.path.splitext(os.path.basename(url))[0])) for
        #     url, _ in self.resources[os.path.basename(self.get_path)])

        os.makedirs(self.get_path, exist_ok=True)

        # download files
        for filename, md5 in self.resources[os.path.basename(self.get_path)]:
            for mirror in self.mirrors:
                url = "{}{}".format(mirror, filename)
                try:
                    print("Downloading {}".format(url))
                    DownLoad().download_and_extract_archive(
                        url, download_root=self.get_path,
                        filename=filename,
                        md5=md5,
                        remove_finished=True
                    )
                except URLError as error:
                    print("Failed to download (trying next):\n{}".format(error))
                    continue
                finally:
                    print()
                break
            else:
                raise RuntimeError("Error downloading {}".format(filename))

    def read_dataset(self):
        """Load data from Mnist dataset file."""
        image_file = 'train-images-idx3-ubyte' if os.path.basename(
            self.get_path) == "train" else 't10k-images-idx3-ubyte'
        data = self.read_image_file(os.path.join(self.get_path, image_file))

        label_file = 'train-labels-idx1-ubyte' if os.path.basename(
            self.get_path) == "train" else 't10k-labels-idx1-ubyte'
        targets = self.read_label_file(os.path.join(self.get_path, label_file))

        return data, targets

    def read_image_file(self, path):
        """Read the image file."""
        x = self.parse_file(path)
        assert x.dtype == np.uint8
        assert x.ndim == 3
        x = x[..., np.newaxis]
        return x

    def read_label_file(self, path):
        """Read the label file."""
        x = self.parse_file(path)
        assert x.dtype == np.uint8
        assert x.ndim == 1
        x = x.astype(np.int32)
        return x

    @staticmethod
    def parse_file(path):
        """Parse the Mnist dataset file."""
        with open(path, "rb") as f:
            data = f.read()
        # parse
        magic = int(codecs.encode(data[0:4], 'hex'), 16)
        nd = magic % 256
        ty = magic // 256
        assert 1 <= nd <= 3
        assert 8 <= ty <= 14
        type_map = {
            8: np.uint8,
            9: np.int8,
            11: np.int16,
            12: np.int32,
            13: np.float32,
            14: np.float64,
        }
        ms_type = type_map[ty]
        s = [int(codecs.encode(data[4 * (i + 1): 4 * (i + 2)], 'hex'), 16) for i in range(nd)]

        num_bytes_per_value = np.iinfo(ms_type).bits // 8
        needs_byte_reversal = sys.byteorder == "little" and num_bytes_per_value > 1
        parsed = np.frombuffer(bytearray(data), dtype=ms_type, offset=(4 * (nd + 1)))
        if needs_byte_reversal:
            parsed = np.flip(parsed, 0)

        return parsed.reshape(*s)
