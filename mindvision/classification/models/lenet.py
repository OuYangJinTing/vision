# Copyright 2021 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""LeNet."""

from typing import Any

from mindvision.classification.models.backbones import Lenet5
from mindvision.classification.models.classifiers import BaseClassifier
from mindvision.classification.utils.model_urls import model_urls
from mindvision.utils.load_pretrained_model import LoadPertainedModel


def lenet(num_classes: int = 10, pretrained: bool = False, **kwargs: Any) -> Lenet5:
    """LeNet structure."""
    backbone = Lenet5(num_classes=num_classes, **kwargs)
    model = BaseClassifier(backbone)

    if pretrained:
        # Download the pre-trained checkpoint file from url, and load
        # checkpoint file.
        arch = "lenet"
        LoadPertainedModel(model, model_urls[arch]).run()
    return model
