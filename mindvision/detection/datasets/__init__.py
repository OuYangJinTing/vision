# Copyright 2021 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""datasets"""
from .coco import COCOYoloDataset
from .coco import COCOSsdDataset
from .yolov5_dataset import COCOYolov5Dataset
from .mindrecord import Coco2MindRecord
from .centerface_generator import CenterfaceDataset
from .centerface_mindrecord import Centerface2MindRecord
from .loadimg import LoadImgFromFile

from .pipelines.formatting import (_Decode, _HWC2CHW, ImgRgbToBgr,
                                   Transpose, EvalFormat, Format, Collect, YOLOv5Format, SsdFormat, SsdDecode,
                                   WiderFaceFormat)
from .pipelines.transforms import (RandomExpand, Resize, ResizeWithinMultiScales,
                                   PilResize, Rescale, RescaleWithoutGT, _Normalize,
                                   StaticNormalize, RandomFlip, RandomPilFlip,
                                   ColorDistortion, ConvertGrayToColor, PerBatchCocoCollect,
                                   YoloBboxPreprocess, PerBatchMap, RandomCropRetinaface,
                                   RetinafaceBboxPreprocess, Ssdpreprocess,
                                   EqualProportionResize, Call_RandomColorAdjust, SsdResizeShape)
from .samplers.distributed_sampler import DistributedSampler
