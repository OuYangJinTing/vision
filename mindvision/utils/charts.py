# Copyright 2021 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""Draw the line graph of accuracy."""

from typing import Optional, Dict, List
import matplotlib.pyplot as plt

font_title = {
    'family': 'Arial',
    'weight': 'bold',
    'size': 14,
}


def accuracy_plot(accuracy_data: Dict,
                  labels: List,
                  save_path: str = './',
                  title: Optional[str] = None,
                  xlabel: Optional[str] = None,
                  ylabel: Optional[str] = None
                  ):
    """
    Plot the graph of accuracy line, xlabel can be network models or iteration nums, ylabel is the accuracy.

    Args:
        accuracy_data(dict): Model accuracy data.
        labels(list): The labels of accuracy line.
        save_path(str): The save path of line chart.
        title(str): The title of graph. Default:None.
        xlabel(str): The Label of x coordinate. Default: None.
        ylabel(str): The Label of y coordinate. Default: None.

    Examples:
        >>> labels = ['MindSpore', 'Pytorch', 'Paper']
        >>> model = ['Resnet18', 'Resnet34', 'Resnet50', 'Resnet101', 'Resnet152']
        >>> data = {labels[0]: {model[0]: 70.078, model[1]: 73.72, model[2]: 76.6, model[3]: 77.62, model[4]: 78.638},
                    labels[1]: {model[0]: 69.758, model[1]: 73.31, model[2]: 76.13, model[3]: 77.374, model[4]: 78.312}}
        >>> accuracy_plot(accuracy_data=data,
                          labels=labels,
                          save_path='./mobilenet_v2_top1_accuracy.png',
                          title='Top-1',
                          xlabel='Resnet',
                          ylabel='ImageNet Top-1 Accuracy(%)')
    """
    # color offers 20 types
    color = ['darkred', 'darkgrey', 'royalblue', 'pink', 'orange',
             'black', 'darkorange', 'forestgreen', 'slategrey', 'lightpink',
             'rosybrown', 'goldenrod', 'mediumturquoise', 'mediumpurple', 'slategray',
             'saddlebrown', 'lawngreen', 'steelblue', 'purple', 'teal']
    # marker offers 20 types
    marker = ['s', '*', 'o', '8', 'v',
              '<', '>', 'h', 'x', '^',
              's', 'P', 'd', 'P', 'D',
              '1', '2', '3', '4', '.']

    for i in range(len(labels)):
        if i >= len(color):
            raise ValueError('The number of labels exceeds 20.')
        x, y = [], []
        for key, value in accuracy_data[labels[i]].items():
            x.append(key)
            y.append(value)
        plt.plot(x, y, color=color[i], marker=marker[i], label=labels[i])

    rotation = 90 if len(x[0]) > 8 else None
    fontsize = 12

    plt.legend(fontsize=fontsize)
    plt.xlabel(xlabel, fontsize=fontsize)
    plt.ylabel(ylabel, fontsize=fontsize)
    plt.xticks(fontsize=fontsize - 2, rotation=rotation)
    plt.yticks(fontsize=fontsize - 2, rotation=rotation)
    plt.title(title, fontdict=font_title)
    plt.grid(linestyle='--')
    plt.savefig(save_path, bbox_inches="tight")
