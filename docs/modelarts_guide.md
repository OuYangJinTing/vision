# ModelArts 开发操作指南

## ModelArts 开发操作指南速览图

<img src="https://i.loli.net/2021/07/26/y8uGk6jDL1WH9st.png" style="zoom: 50%;" />

## 目录

* [ModelArts 开发操作指南](#modelarts-开发操作指南)
    * [ModelArts 开发操作指南速览图](#modelarts-开发操作指南速览图)
    * [目录](#目录)
    * [一、前期准备](#一前期准备)
        * [1\. 获取华为云账号](#1-获取华为云账号)
        * [2\. 登录华为云ModelArts](#2-登录华为云modelarts)
    * [二、ModelArts 之开发调试](#二modelarts-之开发调试)
        * [1\. NoteBook 实例创建](#1-notebook-实例创建)
        * [2\. NoteBook 开发工程概览](#2-notebook-开发工程概览)
        * [3\. 数据集准备](#3-数据集准备)
        * [4\. 工程源码准备](#4-工程源码准备)
        * [5\. Faster\-RCNN 模型开发案例](#5-faster-rcnn-模型开发案例)
            * [[1] 模型说明](#1-模型说明)
            * [[2] 模型训练](#2-模型训练)
            * [[3] 模型评估](#3-模型评估)
            * [[4] 模型推理](#4-模型推理)
    * [三、ModelArts 之训练管理](#三modelarts-之训练管理)
        * [1\. 训练作业创建](#1-训练作业创建)
        * [2\. 训练作业配置说明](#2-训练作业配置说明)
    * [补充材料](#补充材料)
        * [1\. ModelArts之NoteBook创建：OBS和EVS存储方式](#1-modelarts之notebook创建obs和evs存储方式)
            * [[1] NoteBook 创建](#1-notebook-创建)
            * [[2] NoteBook 开发](#2-notebook-开发)
            * [[3] NoteBook 代码调试](#3-notebook-代码调试)
            * [[4] 训练管理作业](#4-训练管理作业)
        * [2\. ModelArts 之本地 IDE 开发(Pycharm)](#2-modelarts-之本地-ide-开发pycharm)
            * [[1] 下载安装Pycharm 专业版](#1-下载安装pycharm-专业版)
            * [[2] NoteBook 实例创建](#2-notebook-实例创建)
            * [[3] Pycharm SSH远程连接](#3-pycharm-ssh远程连接)
            * [[4] Pycharm SSH远程调试及开发](#4-pycharm-ssh远程调试及开发)

## 一、前期准备

### 1. 获取华为云账号

众智开发者**请线下联系华为昇腾官方接口人获取众智专用华为云账号！！！**

其他请按照官网提示进行华为云账号注册

### 2. 登录华为云ModelArts

* 进入华为云官网：[华为云ModelArts登录界面](https://auth.huaweicloud.com/authui/login.html?service=https://account.huaweicloud.com/usercenter/#/login) 参考界面提示登录华为云账号

* 登录成功后，进入[**控制台界面**](https://console.huaweicloud.com/console/?region=cn-north-4#/home)

**Tips: 按照下图搜索并收藏我们需要的服务：这里我们搜索并收藏ModelArts和OBS服务。**
<img src="https://i.loli.net/2021/07/23/wmT7MtVp8yq4IXK.png"  />

## 二、ModelArts 之开发调试

[说明]： **此处以MindDetection套件中Faster-RCNN目标检测模型为例，讲解如何使用 ModelArts 去开发、训练、评估和推理模型**

### 1. NoteBook 实例创建

**Step1**：在华为云官方搜索并点击ModelArts服务，[进入华为云ModelArts服务界面](https://console.huaweicloud.com/modelarts/?agencyId=258b69fa9915473a882786bfdf5a0d28&region=cn-north-4&locale=zh-cn#/dashboard)

**Step2**：点击开发环境下的NoteBook，[进入NoteBook创建界面](https://console.huaweicloud.com/modelarts/?agencyId=258b69fa9915473a882786bfdf5a0d28&region=cn-north-4&locale=zh-cn#/dev-container)

![](https://i.loli.net/2021/07/23/476xJOkALg5VIz1.png)

**Step3**: 点击创建后，我们将看到以下界面：填入自定义名称、推荐开启自动停止功能（节省资费）、选择公共镜像、选择公共资源池、根据镜像类型及模型任务选择Ascend\GPU\CPU类型及规格。

对于存储配置，我们提供两种存储方式，一种是对象存储服务OBS，另一种是云硬盘EVS;

详情可参见：**[1\. ModelArts之NoteBook创建：OBS和EVS存储方式](#1-modelarts之notebook创建obs和evs存储方式)**

**[注意]:** 众智开发者请选择云硬盘 EVS存储方式！！！

**Tips:**

若您希望在NoteBook界面直接开发、调试、训练模型，推荐您使用EVS盘存储方式；

若您希望通过ModelArts中的训练管理及模型管理服务来便捷的开发、管理、部署模型，推荐您使用OBS储存服务;

<img src="https://i.loli.net/2021/07/23/zMsUXGF516nriVS.png"  />

<img src="https://i.loli.net/2021/07/23/1oY8LHcDvsqEOSW.png"  />

**Step4**： 完成NoteBook配置后，按照提示“下一步”并“提交”，完成NoteBook实例的创建。

<img src="https://i.loli.net/2021/07/23/8VvHqlAwnOMKRyt.png"  />

**Step5**： 点击“打开JupyterLab“后，和Anaconda中 Jupyter NoteBook一样，会打开一个网页版的操作界面。

<img src="https://i.loli.net/2021/07/23/5oKhUivOnxjwyPA.png"  />

我们可以选择两种代码开发调式方式：一种是创建NoteBook文件（*.ipynb），另一种是开发者熟悉的Teminal方式。

**Tips：** 通常我们会选择创建NoteBook的方式调试代码

<img src="https://i.loli.net/2021/07/23/pw3a5AJWN4St2L9.png"  />

至此，我们完成一个NoteBook实例的创建。

### 2. NoteBook 开发工程概览

* **EVS存储方式NoteBook工程开发界面**

![](https://i.loli.net/2021/07/23/xFp2SZElo8UtGab.png)

**[注意]:** 可以看到，使用EVS存储方式，和在本地服务器开发类似，所有文件均存储在申请的EVS盘中，用于代码开发、调试、训练等操作都十分方便快捷！**因此对于众智开发者，均建议选择EVS存储方式！**

下面将详细介绍如何快捷的在NoteBook的EVS盘中传入数据集和工程源码，方便开发者进行开发。

* **OBS存储方式NoteBook工程开发界面**

详情请见：[1\. ModelArts之NoteBook创建：OBS和EVS存储方式](#1-modelarts之notebook创建obs和evs存储方式)

### 3. 数据集准备

* **Step1： OBS桶创建**

这里有两种方式：(**众智开发者请选择方式二！！！**)

**Tips:**  需要注意的是，创建的桶需要和您ModelArts服务所在的区域一致！！！，否则在ModelArts中无法访问并读取您创建桶的内容

一、 在[华为云官网的对象存储OBS服务中](https://console.huaweicloud.com/console/?region=cn-north-1#/obs/manager/buckets) 直接创建属于自己的obs桶

![](https://i.loli.net/2021/07/23/eKpXZQ7WC6gVvx5.png)

二、 由于obs桶创建后，在华为云界面上直接上传文件存在大小限制，无法上传大文件，因此**推荐使用OBS Browser Plus 软件进行桶的创建和数据上传**

  下载链接：

  [OBS Browser Plus 软件Windows64位](https://obs-community.obs.cn-north-1.myhuaweicloud.com/obsbrowserplus/win64/OBSBrowserPlus-HEC-win64.zip)

  [OBS Browser Plus 软件Windows32位](https://obs-community.obs.cn-north-1.myhuaweicloud.com/obsbrowserplus/win32/OBSBrowserPlus-HEC-win32.zip)

  [OBS Browser Plus 软件Mac](https://obs-community.obs.cn-north-1.myhuaweicloud.com/obsbrowserplus/mac/OBSBrowserPlus-HEC-mac64.zip)

  下载成功后，按照提示进行安装，使用华为云账号登录后，见到如下界面，则可进行obs桶的创建和其他文件数据操作。

![](https://i.loli.net/2021/07/23/xRYp6Om45aoCTen.png)

* **Step2： OBS桶内创建相关文件夹 （使用EVS存储方式只需要创建dataset数据集文件即可！！！）**

由于后续在ModelArts平台上使用OBS存储方式进行模型开发时，需要提前在OBS中存储数据集、工程代码文件、训练/评估/推理输出结果等文件，**因此建议提前在自己的obs桶中建立相关文件夹。**

这里请按照如下目录格式在自己的obs桶内创建文件夹：

![](https://i.loli.net/2021/07/23/7JBA3NYhrzD9jE5.png)

* **Step3： 下载并上传数据集文件至obs桶**

**常见目标检测数据集汇总**：

COCO：[点击下载](https://cocodataset.org/#home)        VOC：[点击下载](http://host.robots.ox.ac.uk/pascal/VOC/voc2012/VOCtrainval_11-May-2012.tar)        WIDER Face： [点击下载](http://shuoyang1213.me/WIDERFACE/)        Cityscapes：[点击下载](https://www.cityscapes-dataset.com/login/)

* **Step4： 拷贝数据集至NoteBook挂载的EVS盘**

这里需要开发者提前了解华为提供的MoXing语法，学习链接：[MoXing开发指南](https://support.huaweicloud.com/moxing-devg-modelarts/modelarts_11_0001.html)

下面给出在NoteBook中执行的代码：

```python
import moxing as mox
# 存储数据集的obs路径
obs_data_path = 'obs://your_bucket_name/dataset'
# 列出obs数据集路径下的文件
mox.file.list_directory(obs_data_path)
# 保存至notebook工作目录下
save_path = "/home/ma-user/work/dataset"
# 执行copy文件夹操作
mox.file.copy_parallel(obs_data_path, save_path)
```

至此，我们已经完成了数据集的准备工作！

### 4. 工程源码准备

* 下载或克隆官方开源mindvision代码库

  **两种方式：**

    * 代码下载链接：[mindvision官方代码库](https://gitee.com/mindspore/mindvision/repository/archive/master.zip) 可直接下载至本地仓
    * `git clone https://gitee.com/mindspore/mindvision.git`

**Tips**：
**若您使用EVS存储方式，推荐使用 git clone 方式直接克隆至NoteBook工作目录下**；

**若您使用OBS存储方式，推荐使用官网下载并上传至OBS桶后同步至NoteBook工作目录下**；

* 这里给出在NoteBook中使用 git clone 的方式：（***NoteBook中脚本命令需要加  "!" 号***）

```python
# 检查当前目录 (通常在 /home/ma-user/work/ 目录下)
!pwd
!cd /home/ma-user/work/
```

```python
# 拉取mindvision源码
!git clone https://gitee.com/mindspore/mindvision.git

# 将工作路径切换至detection源码仓
import os
os.chdir("mindvision/mindvision/detection")
```

```python
# 检查当前路径
!pwd
```

  **Tips：** 同样的，如果您想使用Terminal方式开发，亦可参考上述流程，完成 minddetection 安装工作

### 5. Faster-RCNN 模型开发案例

**【注意】：** 在阅读本章节之前，需要开发者提前学习MindDetection套件的使用方法，了解 [MindDetection套件的总体框架设计](https://gitee.com/mindspore/mindvision/mindvision/detection/docs/MindVision%20Detection%20introduction.md)

该章节主要讲述**如何使用NoteBook完成模型的开发、调试、训练等，但为了方便更多习惯于本地 IDE 的开发者，如Pycharm，ModelArts上还提供了远程SSH开发功能，使用方式可参见文末补充说明： [2\. ModelArts 之本地 IDE 开发(Pycharm)](#2-modelarts-之本地-ide-开发pycharm)**

#### [1] 模型说明

MindDetection套件中已经集成了Faster-RCNN模型的训练、评估及推理功能，供开发者参考进行其他模型的开发工作。

如何使用MindDetection套件的Faster-RCNN模型进行推理、评估及推理，请参见 [Faster-RCNN官方说明文档](https://gitee.com/mindspore/mindvision/mindvision/detection/README.md)

#### [2] 模型训练

* **Step1：** 进入mindvision中的detection目录下

  ![](https://i.loli.net/2021/07/23/SrBMt2ldWQkiUpc.png)

* **Step2:** 配置faster-rcnn的configs文件，进入configs文件夹

    * 首先，配置configs/base/datasets/faster_rcnn_dataset.yaml文件，需要按照数据集实际路径，更改数据集加载路径！

    <img src="https://i.loli.net/2021/07/23/qHgFvhWOy7B4nAK.png"  />

    * 其次，配置configs/base/models/faster_rcnn_fpn.yaml文件，按照训练需求指定训练参数。

    ![](https://i.loli.net/2021/07/23/bQIgrtEGZ1hHN8m.png)

    * 其他参数配置请参见官方说明文档：[MindDetection配置文件参数详解](https://gitee.com/mindspore/mindvision/mindvision/detection/docs/detection%20config.md)

* **Step3：** 在detection目录下创建your_notebook.ipynb文件，按照下述流程进行代码的训练及调试。

```python
# 开启训练调试脚本命令
!python tools/train.py --config configs/faster_rcnn/faster_rcnn_r50_fpn.yaml --device_id 0
```

```python
# Starting ModelArts Training Task
## 代码调试训练成功后，需使用ModelArts上的训练管理服务作业启动训练任务
## 拷贝工程代码至自己的 obs 桶中
import moxing as mox
src_path = "/home/ma-user/work/mindvision"
### your_bucket_name 请替换成自己的 obs 桶  V0001是版本号 eg.V0002 ...
obs_path = "obs://your_bucket_name/src/V0001/mindvision"
### copy整个工程文件到 obs 桶中
mox.file.copy_parallel(src_path, obs_path)
### 检查是否拷贝成功
print(mox.file.list_directory(obs_path))
```

* **Step4:**  使用ModelArts训练管理作业服务，创建faster-rcnn模型的训练任务。

为了使开发者更加清楚地了解ModelArts训练管理作业的运行机制，文档单独为ModelArts的训练管理服务做了详细阐述，详细内容请阅读：

  **[三、ModelArts 之训练管理](#三modelarts-之训练管理)**

  请开发者按照上述文档流程，完成faster-rcnn模型训练任务的创建！！！相关输出均会保存在obs://your_bucket_name/train_out目录下。

* **Step5：** 训练完成后，可将训练输出的模型copy到当前NoteBook的工程目录下，进行评估和推理测试。

    * 拷贝obs中保存的训练模型

```python
# 从obs桶中copy训练任务输出的模型到当前目录下：
import moxing as mox
## obs 保存的训练模型路径
obs_ckpt_path = "obs://your_bucket_name/train_out/V_{your_train_vision_number}/outputs/checkpoints/best_faster_rcnn.ckpt"
## 保存至notebook的工程路径
save_ckpt_path = "/home/ma-user/work/mindvision/mindvision/detection/best_faster_rcnn.ckpt"
## 执行文件copy
mox.file.copy(obs_ckpt_path, save_ckpt_path)
```

#### [3] 模型评估

```python
# 开启模型评估脚本
!python tools/eval.py --config configs/faster_rcnn/faster_rcnn_r50_fpn.yaml \
                      --ann_file /home/ma-user/work/dataset/annotations/instances_val2017.json \
                      --checkpoint_path best_faster_rcnn.ckpt \
                      --work_dir outputs
```

#### [4] 模型推理

```python
# 开启模型推理脚本
!python tools/predict.py --config configs/faster_rcnn/faster_rcnn_r50_fpn.yaml \
                         --img_file ./pictures  \
                         --checkpoint_path best_faster_rcnn.ckpt \
                         --work_dir outputs \
                         --save_dir det_results
```

## 三、ModelArts 之训练管理

### 1. 训练作业创建

* **训练作业配置**

<img src="https://i.loli.net/2021/07/23/8R7Mla94XrZ2pgh.png"  />

![](https://i.loli.net/2021/07/23/8LHVrjPKCuWgfUQ.png)

其中标红的配置参数的写法具有规律性，请参见 [2\. 训练作业配置说明](#2-训练作业配置说明)

* **训练任务运行**

  ![](https://i.loli.net/2021/07/23/lQhZbwNrSuk3CsH.png)

  这里可通过日志实时查看模型训练的状态输出，方便开发者debug训练代码。另外还可以查看资源占用情况，分析工程代码对服务器资源的利用率。

### 2. 训练作业配置说明

**【注意】：** 需要开发者了解以下几点：

* **1、由于训练服务开启时，ModelArts会自动申请一块新的内存环境，将指定的obs代码目录下所有文件copy到/home/work/user-job-dir/目录下，即会在内部自动执行以下命令：**

```python
import moxing as mox
src_url = "s3://your_bucket_name/src/mindvision" # 和"obs://..."等价
cur_job_path = "/home/work/user-job-dir/mindvision"
mox.file.copy_parallel(src_url, cur_job_path)
```

* **2、启动训练脚本train.py参数配置需要和相应的config文件中配置路径一致，即在上面configs文件中针对faster-rcnn的配置文件路径均要做相应更改，比如：**

```text
/detection/configs/base/datasets/faster_rcnn_datasets.yaml文件中:

ann_file："/home/ma-user/work/dataset/annotations/instances_train2017.json"  更改为：

ann_file："/home/work/user-job-dir/dataset/annotations/instances_train2017.json"

其他路径同理，全部更改到实际训练时的工程路径即可!!!
```

* **3、训练启动脚本train.py代码的写法也具有规范性，为正常使用ModelArts的训练管理作业，其中“必选参数”是必须写在train.py的参数输入里！下面给出train.py参数配置部分的代码函数：**

```python
def parse_arguments():
  """parse train arguments"""
  parser = argparse.ArgumentParser(description='Train a detector')
  parser.add_argument("--data_url", type=str, default="", help="Dataset path for train on ModelArts platform")
  parser.add_argument("--train_url", type=str, default="", help="Train file outputs path on ModelArts platform")
  parser.add_argument("--train_data", type=str, default="", help="Train file outputs path on ModelArts platform")
  parser.add_argument('--is_modelarts', type=bool, default=False, help='Whether to run on the modelarts platform')
  parser.add_argument('--config', type=str, default="", help='train config file path')
  parser.add_argument('--work_dir', default='./',
                      help='the path to save logs and models')
  parser.add_argument('--device_id',
                      help='device id')
  parser.add_argument('--seed', default=1,
                      help='the random seed')
  parser.add_argument(
      '--options',
      nargs='+',
      action=ActionDict,
      help='override some settings in the used config, the key-value pair'
           'in xxx=yyy format will be merged into config file')

  args = parser.parse_args()
  return args
```

另外，即使在ModelArts上创建训练任务时指定了obs数据集的路径，但MindSpore系列的源码目前并不支持直接从obs桶中读取数据集，而是采用MoXing框架将obs桶内数据集copy到训练的工程目录下，即会执行以下代码：

```python
# if the code runs in ModelArts, copy train dataset to ModelArts Training Workspace
if args.is_modelarts:# 当运行在ModelArts训练管理作业上时
    if not os.path.exists(args.train_data): # 在创建任务时指定的数据集路径，需要和config文件中配置一致
        os.makedirs(args.train_data)
    mox.file.copy_parallel(args.data_url, args.train_data) # 将obs桶内的数据集整个copy到ModelArts训练作业的工作目录下
```

* **4、关于训练输出文件的保存问题，由于ModelArts不会自动将输出的模型文件自动保存至指定的obs路径下，因此MindDetection中训练脚本train.py提供了该功能，会自动将整个训练输出的文件夹：/home/work/job-user-dir/mindvision/mindvision/detection/outputs 整个copy到：**

  **obs://your_bucket_name/train_out/V_{your_train_vision_number}/outputs**

  即会执行以下代码：

```python
# Apply for ModelArts Training output and save files config
if args.is_modelarts:# 当运行在ModelArts训练管理作业上时
    obs_work_path = os.path.join(args.train_url, "outputs")
    if not mox.file.exists(obs_work_path):
        mox.file.make_dirs(obs_work_path)
    mox.file.copy_parallel(args.work_dir, obs_work_path)
```

  因此在**训练完成后，开发者可在自己的obs桶中查看训练输出文件，包括保存的模型、训练日志等信息**

## 补充材料

### 1. ModelArts之NoteBook创建：OBS和EVS存储方式

通过章节 **[二、ModelArts 之开发调试](#二modelarts-之开发调试) 和 [三、ModelArts 之训练管理](#三modelarts-之训练管理)， 开发者可以充分了解创建NoteBook时使用EVS存储方式开发、调试、训练AI工程的过程，然而对于ModelArts的AI开发者来讲，OBS存储方式也是需要了解并熟悉的，本章节将会详细讲述使用OBS存储方式进行开发的流程，从中你可以感受到和EVS有很多共通之处，也会有一些不同，仅供开发者参考**

#### [1] NoteBook 创建

参考ModelArts开发案例中的 [1\. NoteBook 实例创建](#1-notebook-实例创建) ， 仅在"存储配置"选项处选择OBS存储方式即可，其余选项根据项目需求进行选择！

![](https://i.loli.net/2021/07/23/iBcvUTt6gXYe5Wx.png)

#### [2] NoteBook 开发

* 根据 [4\. 工程源码准备](#4-工程源码准备) ，使用OBS存储方式需要去官网下载工程源码，并上传至obs://your_bucket_name/src/目录下，则会自动在NoteBook开发界面显示mindvision的源码

  **【注意】**：

    * 和EVS存储方式有个明显的区别，OBS存储方式的开发工作目录会直接挂载在创建时指定的obs桶的目录下：obs://your_bucket_name/src/，因此在该目录下上传的文件会直接显示在NoteBook开发界面下；
    * NoteBook开发界面创建、上传、修改等操作后的文件也会自动同步到obs://your_bucket_name/src/目录下；
    * 由于挂载的是obs目录，其文件并不在NoteBook的工程环境下，因此需要使用Sync同步功能将obs目录下文件同步至NoteBook的工作环境下，方便开发者使用NoteBook的训练资源，这是与EVS存储的不同之一；
    * Sync同步功能最多只能同步5G文件，若NoteBook工作目录下已存在3G文件，则最多能同步2G文件，如果需要传入更大的文件（如数据集），则需要使用moxing框架，将obs目录下文件直接copy到NoteBook工作目录下，这一点与使用EVS存储方式是类似的；

![](https://i.loli.net/2021/07/23/EBN4OuJTfVUPQn5.png)

#### [3] NoteBook 代码调试

开发者在每次修改好代码后，可通过Sync同步按钮将最新文件同步至NoteBook目录下，同时使用Terminal方式运行源码中的相关代码进行调式。

对于数据集的传入，参考 [3\. 数据集准备](#3-数据集准备) ，使用moxing语法将数据集手动copy至NoteBook环境下，在调试结束后，需手动删除数据集，避免Sync同步功能无法使用。

**建议使用如下NoteBook执行流程**：

```python
# Step1：数据集copy
import moxing as mox
obs_dataset_path = "obs://your_bucket_name/dataset"
notebook_dataset_path = "/home/ma-user/work/dataset"
mox.file.copy_parallel(obs_dataset_path, notebook_dataset_path)
```

```python
# Step2: 调试代码，以faster-rcnn的train.py为例
!cd /home/ma-user/work/mindvision/mindvision/detection
# 根据数据集实际目录更改faster_rcnn_r50_fpn.yaml配置文件中路径
!python tools/train.py --config configs/faster_rcnn/faster_rcnn_r50_fpn.yaml --device_id 0
```

```python
# Step3: 释放数据集内存
!rm -r /home/ma-user/work/dataset
```

#### [4] 训练管理作业

在NoteBook上完成调通代码后，需要开发者使用ModelArts的训练管理服务，在上面创建训练作业任务，完成模型的训练。此处和EVS存储方式使用ModelArts训练管理服务的流程基本相同。由于代码整个直接保存在obs桶中，因此和EVS不同的是，**它不再需要从EVS盘中将调式好的工程代码手动copy到obs://your_bucket_name/src/目录下，创建训练任务时可直接指定obs代码路径！！！**

详细操作流程请见：[三、ModelArts 之训练管理](#三modelarts-之训练管理)

### 2. ModelArts 之本地 IDE 开发(Pycharm)

#### [1] 下载安装Pycharm 专业版

请点击 [Pycharm官方下载地址](https://www.jetbrains.com/pycharm/download/other.html) 下载并安装到本地（**Pycharm版本>2019.2 专业版**）

#### [2] NoteBook 实例创建

参考ModelArts开发案例中的 [1\. NoteBook 实例创建](#1-notebook-实例创建)，在最后一栏中**打开SSH远程开发功能**并创建密钥对和远程访问白名单。

注意**要保存好创建生成的密钥对（第一次生成创建时有下载界面，之后不再提供下载），SSH远程登录时需要使用**

![](https://i.loli.net/2021/07/26/ILmSFuW6Ejazpew.png)

#### [3] Pycharm SSH远程连接

* **Step1：** 创建NoteBook成功后，点击创建的NoteBook实例，查看远程访问信息，主要关注图中标红的信息，包括远程访问的用户名、Host地址、端口号、访问机器的IP地址。

  ![](https://i.loli.net/2021/07/26/JZqYOWQlUeLaANv.png)

* **Step2：** 打开Pycharm的Setting选项，选择Tools下的SSH Configuration，按照上述信息，填写SSH远程访问选项。

  ![](https://i.loli.net/2021/07/26/ibI3a2D9ehRCBYO.png)

* **Step3：** 利用Pycharm打开远程访问的挂载目录，方便开发者实时进行开发上传。

    * 打开Tools下的Deployment的Configuration选项

    ![](https://i.loli.net/2021/07/26/WUXvyzEpCiFh6MN.png)

    * 配置远程NoteBook访问目录

    ![](https://i.loli.net/2021/07/26/gpi34v1nkWTKJs6.png)

    * 打开远程NoteBook工程目录

    ![](https://i.loli.net/2021/07/26/HDFoI25ZykhExfA.png)

至此，相信开发者能够很顺利的完成SSH远程连接NoteBook，下面将讲述如何利用Pycharm来进行工程开发。

#### [4] Pycharm SSH远程调试及开发

在ModelArts的官方使用手册中，已经详细讲述了如何设置Pycharm来利用NoteBook进行远程开发，这里给出使用文档链接。

请主要阅读：[本地IDE开发之Pycharm配置](https://support.huaweicloud.com/engineers-modelarts/modelarts_30_0015.html)

从中我们可以了解到以下几点：

* **利用Pycharm获取NoteBook开发环境预置的虚拟环境路径**
* **利用Pycharm配置ModelArts上的Python Interpreter**
* **ModelArts中NoteBook环境依赖库安装**
* **利用Pycharm在开发环境中调试代码**